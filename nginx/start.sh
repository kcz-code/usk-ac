#!/usr/bin/env bash

set -e

if [ -e "/etc/letsencrypt/nginx.conf" ]; then
  cp "/etc/letsencrypt/nginx.conf" /etc/nginx/
  cron
else
  echo "Missing certbot's nginx.conf"
  echo "  Obtain new SSL certificates:"
  echo "    docker compose run --rm -p 80:80 nginx certbot-run"
  exit 1
fi

exec nginx -g "daemon off;"
