class GroupMailer < ApplicationMailer

  def ml_invitation(ml, content)
    @ml = ml
    @content = content
    mail.to = ml.users.map { |u| u.email }
  end

  def email_confirmation(user, host)
    @url = host + '/confirm/' + user.confirm_hash
    mail.to = user.email
    mail.subject += 'E-Mail-Bestätigung'
  end

  def user_wait_for_admin(user)
    @user = user
    mail.to = user.email
    mail.subject += 'E-Mail bestätigt'
  end

  def admin_new_candidate(user, host)
    @user = user
    @url = host + '/users/' + user.id.to_s
    mail.to = User.all.filter { |u| u.admin? }.map { |u| u.email }
    mail.subject += 'Neuer Kandidat'
  end

  def user_welcome(user, host)
    @user = user
    @url = host + '/login'
    mail.to = user.email
    mail.subject += 'Wilkommen'
  end

  def password_forgot(user, host)
    @user = user
    @url = host + '/password/' + user.confirm_hash
    mail.to = user.email
    mail.subject += 'Passwort' 
  end

  def user_message_notification
    message_notification params[:msg_id]
  end

  def group_message_notification
    message_notification params[:msg_id]
  end

  private

  def message_notification(msg_id)
    msg = Message.find msg_id
    if msg
      subject = 'UskAC: '
      to = nil

      @msg = msg.content
      if msg.recipient
        subject += msg.user.name
        to = msg.user.email
      elsif msg.group
        subject += "Gruppe #{msg.group.name}"
        to = msg.group.users.pluck :email
      end
      mail(to: to, subject: subject, template_name: 'message_notification')
    end
  end

end
