class ApplicationMailer < ActionMailer::Base
  default from: 'info@usk-ac.de'
  default subject: 'Usk-AC: '
  layout "mailer"
end
