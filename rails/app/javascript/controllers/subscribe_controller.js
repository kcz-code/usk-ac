import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  connect() {
    if (!navigator.serviceWorker || !window.PushManager) {
      console.warn("No Push Notification possible");
    } else if (Notification.permission === "denied") {
      console.warn("Push Notifications denied");
    } else if (Notification.permission === "granted") {
      console.info("Push Notifications granted");
      this.register_and_subscribe();
    } else {
      console.log("Push Notifications not activated yet");

      this.element.classList.remove('d-none');

      this.element.querySelector('button').addEventListener("click", event => {
        event.stopPropagation();
        event.preventDefault();

        this.element.classList.add('d-none');

        (async function (that) {

          const permission = await Notification.requestPermission();

          if (permission === "granted") {
            await that.register_and_subscribe();
          } else {
            console.warn("Push Notifications: ", permission);
          }

        })(this)
        .catch(error => {
          console.error(`Web Push subscription failed: ${error}`)
        });
      });

    }
  }

  async register_and_subscribe() {
    const registration = await navigator.serviceWorker.register("/service_worker", {scope: "/" });
    console.log("ServiceWorker registration successful with scope: ", registration.scope);
    
    const key = this.urlB64ToUint8Array(this.element.dataset.key);

    const subscription = await registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: key
    });
    console.log("PushManager subscription successful with endpoint: ", subscription.endpoint);

    await fetch("/push_subscriptions", {
      method: "POST",
      headers: {
          "Content-Type": "application/json",
      },
      body: JSON.stringify(subscription)
    });
  }

  urlB64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, "+")
      .replace(/_/g, "/");
    const rawData = atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
}