import "bootstrap"

addEventListener("turbo:load", (event) => {
  // tooltips
  const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
  [...tooltipTriggerList].map(tooltipTriggerEl => bootstrap.Tooltip.getOrCreateInstance(tooltipTriggerEl, { delay: { "show": 1000, "hide": 100 } }));
});
