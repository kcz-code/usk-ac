class UsersController < ApplicationController

  def login
  end

  def new
    @user = User.new
  end

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Benutzer '#{@user.name}' wurde zugefügt"
      log "Neuer Benutzer '#{@user.name}', id: #{@user.id}"
      redirect_to users_path
    else
      render 'new'
    end
  end

  def update
    @user = User.find(params[:id])
    u_params = administration? ? admin_params : user_params
    if administration?
      if u_params[:group_ids]
        u_params[:group_ids] = u_params[:group_ids].map {|id| id.to_i}.delete_if {|id| id <= 0}
      end
      if u_params[:clear_messages]
        u_params.extract! :clear_messages
        u_params[:messages] = []
      end
    end
    if @user.update(u_params)
      flash.now[:success] = "Benutzer wurde aktualisiert"
      log "Benutzer '#{@user.name}' wurde aktualisiert"
      redirect_to users_url
    else
      flash.now[:danger] = "Kann Benutzer nicht aktualisieren"
      render_toast
    end
  end

  def destroy
    user = User.find(params[:id])
    if administration? && user && current_user.id != user.id && user.name != 'admin'
      if user.destroy
        flash[:success] = "Benutzer '#{user.name}' wurde gelöscht"
        log "Benutzer gelöscht '#{user.name}', id: #{user.id}"
        redirect_to users_url
        return
      end
    end
    flash.now[:danger] = "Kann Benutzer '#{user ? user.name : '?'}' nicht löschen"
    render_toast
  end

  def accept
    user = User.find(params[:id])
    if current_user.admin? && user && user.candidate?
      user.user = true
      if user.save
        GroupMailer.user_welcome(user, request_url).deliver
        flash[:success] = "Kandidat wurde akzeptiert"
        log "Kandidat wurde akzeptiert '#{user.name}'"
        redirect_to users_url
      else
        flash.now[:danger] = "Kandidat wurde nicht akzeptiert!"
        render_toast
      end
    else
      redirect_to root_url
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def admin_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation,
                      :admin, :confirmed, :user, :clear_messages, :group_ids => [])
  end

end
