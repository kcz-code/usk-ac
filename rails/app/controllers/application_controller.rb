class ApplicationController < ActionController::Base
  before_action :require_login

  protect_from_forgery with: :exception
  include SessionsHelper
  include LogHelper

  protected

  def render_toast
    render turbo_stream: turbo_stream.update("toast", partial: "layouts/toast")
  end

  def request_url
    request.protocol + request.host_with_port
  end

  private 

  def require_login
    unless current_user
      redirect_to root_url
    end
  end

end
