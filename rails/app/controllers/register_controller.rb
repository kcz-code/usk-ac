class RegisterController < ApplicationController

  skip_before_action :require_login

  def new
  end

  def create
    @user = User.new(register_params)
    @user.confirm_hash = SecureRandom.uuid
    @user.confirm_at = 5.hours.from_now       # email confirmation within 5 hours
    @user.user = false

    alredy_exists = User.find_by(name: @user.name) || User.find_by(email: @user.email)

    if !alredy_exists && @user.save
      GroupMailer.email_confirmation(@user, request_url).deliver
      log "Neuer Bewerber '#{@user.name}' hat sich registriert"
      render_register_confirmation
    else
      flash.now[:danger] = "Registrierung nicht möglich"
      render_toast
    end
  end

  def confirm
    @user = User.find_by_confirmation params[:id]
    if @user
      @user.confirmed = true
      @user.save
      @user.reset_confirmation
      GroupMailer.user_wait_for_admin(@user).deliver
      GroupMailer.admin_new_candidate(@user, request_url).deliver
      log "Bewerber '#{@user.name}' hat seine EMail-Adresse bestätigt"
    else
      redirect_to root_url
    end
  end

  private

  def render_register_confirmation
    render turbo_stream: turbo_stream.update("confirmation", partial: "register/register_confirmation")
  end

  def register_params
    params.require(:register).permit(:name, :email, :password,
                                 :password_confirmation)
  end

end
