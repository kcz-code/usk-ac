class GroupsController < ApplicationController

  def new
    @group = Group.new
  end

  def index
    @groups = Group.all
  end

  def show
    @group = Group.find(params[:id])
  end

  def create
    @group = Group.new(ml_params)
    if @group.save
      flash[:success] = "Gruppe wurde zugefügt"
      log "Gruppe '#{@group.name}' wurde zugefügt"
      redirect_to groups_url
    else
      flash.now[:danger] = "Kann Gruppe nicht erstellen"
      render_toast
    end
  end

  def destroy
    group = Group.find(params[:id])
    group.destroy
    flash[:success] = "Gruppe '#{group.name}' wurde gelöscht"
    log "Gruppe '#{group.name}' wurde gelöscht"
    redirect_to groups_url
  end

  def edit
    @group = Group.find(params[:id])
    @users = User.all
  end

  def update
    @group = Group.find(params[:id])
    if @group.update(ml_params)
      flash[:success] = "Gruppe wurde aktualisiert"
      log "Gruppe '#{@group.name}' wurde aktualisiert"
      redirect_to groups_url
    else
      render 'edit'
    end
  end

  def add_remove_user
    @group = Group.find(params[:id])
    user = User.find(params[:user_id])
    if @group && user
      if @group.users.include? user
        @group.users.delete user
        log "Mitglied '#{user.name}' wurde aus der Gruppe '#{@group.name}' entfernt"
      else
        @group.users.push user
        log "Mitglied '#{user.name}' wurde der Gruppe '#{@group.name}' hinzugefügt"
      end
      @group.save
      flash.now[:success] = "Gruppe wurde aktualisiert"
      render 'show'
    else
      redirect_to groups_url
    end
  end

  def ml_params
    params.require(:group).permit(:name, :description, :user_ids => [])
  end

end
