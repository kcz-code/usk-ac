class PasswordController < ApplicationController

  skip_before_action :require_login

  def edit
    @user = User.find_by_confirmation params[:id]
    redirect_to root_url unless @user && @user.user?
  end

  def update
    p = params.require :password
    @user = User.find_by(confirm_hash: p[:id])
    if @user
      p = p.permit(:password, :password_confirmation)
      if p[:password].empty?
        flash.now[:danger] = "Passwort kann nicht leer sein"
        render_toast
      elsif @user.update(p)
        @user.reset_confirmation
        log_in @user
        flash[:success] = "Passwort wurde geändert"
        log "Password von Benutzer '#{@user.name}' wurde geändert"
        redirect_to @user
      else
        render 'edit'
      end
    else
      redirect_to root_url
    end
  end

end
