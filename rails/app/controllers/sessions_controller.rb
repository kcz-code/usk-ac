class SessionsController < ApplicationController

  skip_before_action :require_login

  def new
  end

  def create
    sp = session_params
    email = sp[:email].downcase
    user = User.find_by(email: email)
    unless user
      users = User.where_name_like sp[:email], 2
      user = users.first if users.size == 1
    end
    if user
      if sp[:forgot] == '1' && user.user?
        user.confirm_hash = SecureRandom.uuid
        user.confirm_at = 5.hours.from_now       # confirmation within 5 hours
        user.save
        GroupMailer.password_forgot(user, request_url).deliver
        flash.now[:success] = "EMail wurde verschickt"
        render_toast
        return
      elsif user.authenticate(sp[:password])
        log_in user
        redirect_to messages_url
        return
      end
    end
    flash.now[:danger] = "Anmeldung war nicht erfolgreich"
    render_toast
  end

  def destroy
    log_out
    redirect_to root_url
  end

  def administration
    if logged_in? && current_user.admin?
      session[:administration] = !session[:administration]
      if administration?
        flash[:success] = "Administrationsmodus ist aktiv"
      else
        flash[:warning] = "Administrationsmodus ist beendet"
      end
      redirect_to root_url
    else
      flash.now[:danger] = "Das ist nicht möglich"
      render_toast
    end
  end

  private

  def session_params
    params.require(:session).permit(:email, :password, :forgot)
  end

end
