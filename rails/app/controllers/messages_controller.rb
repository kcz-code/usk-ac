class MessagesController < ApplicationController

  def new
    @msg = Message.new
    @recipient = ''
    @msg.user = current_user
  end

  def group_new
    @msg = Message.new
    group = Group.find params[:id]
    @recipient = group ? group.name : ''
    @msg.user = current_user
    render 'new'
  end

  def new_user
  end

  def index
    current_user.notifications.delete_all
    
    @msgs = Message.all.order('created_at DESC')
    unless administration?
      me = current_user
      @msgs = Message.all.order('created_at DESC').select { |msg| msg.group || msg.recipient == me || msg.user == me }
    end
  end

  def show
    @msg = Message.find(params[:id])
  end
  
  def create
    recipient = (params[:recipient] || '').strip
    users = User.where_name_like recipient, 2
    groups = Group.where_name_like recipient, 2
    if (users.size + groups.size) == 1
      @msg = Message.new msg_params
      @msg.group_id = groups.first ? groups.first.id : nil
      @msg.recipient_id = users.first ? users.first.id : nil
      @msg.user_id = current_user.id

      if @msg.save
        flash[:success] = "Mitteilung wurde gespeichert"
        if @msg.group
          GroupMessageNotifier.with(msg_id: @msg.id).deliver current_user
          log "Neue Gruppen-Mitteilung an '#{@msg.group.name}'"
        else
          UserMessageNotifier.with(msg_id: @msg.id).deliver current_user
          log "Neue Benutzer-Mitteilung an '#{@msg.user.name}'"
        end
        redirect_to messages_url
      else
        flash.now[:danger] = "Speichern nicht möglich"
        render_toast
      end
    else
      flash.now[:danger] = "Empfänger nicht eindeutig"
      render_toast
    end
  end

  def edit
    @msg = Message.find(params[:id])
  end

  def update
    @msg = Message.find(params[:id])
    if @msg.update(msg_params)
      flash[:success] = "Mitteilung wurde aktualisiert"
      log "Mitteilung '#{@msg.group ? @msg.subject : @msg.id}' wurde aktualisiert"
      redirect_to messages_url
    else
      flash.now[:danger] = "Speichern nicht möglich"
      render_toast
    end
  end

  def destroy
    msg = Message.find(params[:id])
    if msg.destroy
      flash[:success] = "Mitteilung '#{msg.subject}' wurde gelöscht"
      log "Mitteilung '#{msg.group ? msg.subject : msg.id}' wurde gelöscht"
      redirect_to messages_url
    else
      flash.now[:danger] = "Löschen nicht möglich"
      render_toast
    end
  end

  private

  def msg_params
    params.require(:message).permit(:content, :user_id, :group_id)
  end

end
