class NamesSearchController < ApplicationController

  HIT_COUNT = 5

  def search
    query = params[:query]
    users = User.where_name_like query, HIT_COUNT
    groups = Group.where_name_like query, HIT_COUNT - users.size
    render json: (groups + users).pluck(:name)
  end

end
