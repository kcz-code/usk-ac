VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/

class User < ApplicationRecord
  before_save { 
    self.email = email.downcase
  }
  
  validates :name, presence: true, length: { maximum: 50 }

  validates :email, presence: true, length: { maximum: 255 },
     format: { with: VALID_EMAIL_REGEX },
     uniqueness: { case_sensitive: false }

  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, if: :password_validation

  has_and_belongs_to_many :groups
  has_many :messages, dependent: :destroy
  has_many :push_subscriptions, dependent: :destroy
  has_many :notifications, as: :recipient, dependent: :destroy, class_name: "Noticed::Notification"

  def self.find_by_confirmation(id)
    user = self.find_by confirm_hash: id
    if user && user.confirm_at < DateTime.now
      user.reset_confirmation
      user = nil
    end
    user
  end

  def reset_confirmation
    self.confirm_hash = nil
    self.confirm_at = nil
    self.save
  end

  def password_validation
    new_record? || password_digest_changed?
  end

  def admin?
    self.user && self.admin
  end

  def user?
    self.user
  end

  # registered but not yet confirmed
  def registered?
    !self.user && !self.confirmed && self.confirm_hash 
  end

  # registered and confirmed (waiting for admin's approval)
  def candidate?
    !self.user && self.confirmed 
  end

  def display_role
    if self.admin?
      return 'Moderator'
    elsif self.user?
      return 'Benutzer'
    elsif self.candidate?
      return 'Kandidat'
    else
      return 'Bewerber'
    end
  end

  def clear_messages
    false
  end

end
