class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def self.where_name_like(text = '', count = 1)
    self.limit(count).where("lower(name) LIKE ?", "%" + (text || '').strip.downcase + "%")
  end

end
