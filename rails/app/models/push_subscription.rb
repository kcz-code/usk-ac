class PushSubscription < ApplicationRecord

  validates :endpoint, presence: true, uniqueness: true

  belongs_to :user

  def publish(data)
    WebPush.payload_send(
      message: data.to_json,
      endpoint: endpoint,
      p256dh: p256dh_key,
      auth: auth_key,
      vapid: {
        private_key: ENV['VAPID_PRIVATE'],
        public_key: ENV['VAPID_PUBLIC']
      }
    )
  end

end
