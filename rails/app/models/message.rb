class Message < ApplicationRecord

  validates :content,  presence: true, length: { maximum: 2048 }

  belongs_to :user
  belongs_to :group, optional: true
  belongs_to :recipient, optional: true, class_name: 'User'

  def subject
    self.content.truncate 50
  end

end
