class Group < ApplicationRecord
  validates :name,  presence: true, length: { maximum: 50 },
     uniqueness: { case_sensitive: false }

  validates :description,  presence: true, length: { maximum: 1024 }

  has_and_belongs_to_many :users
  has_many :messages

end
