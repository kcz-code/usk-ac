module LogHelper

  def log(content)
    log = Log.new
    log.user = current_user
    log.content = content
    log.save
  end

end
