class DeliveryMethods::WebPush < Noticed::DeliveryMethods::Base
  def deliver
    msg = Message.find notification.params[:msg_id]
    if msg
      data = { title: 'UskAC: ', body: msg.content.truncate(100) }
      if msg.recipient
        data[:title] += msg.user.name
        self.push_subscription msg.recipient, data
      elsif msg.group
        data[:title] += "Gruppe #{msg.group.name.truncate 20}"
        msg.group.users.each do |user|
          self.push_subscription user, data
        end
      end
    end
  end

  def push_subscription(recipient, data)
    recipient.push_subscriptions.each do |subscription|
      subscription.publish(data)
      rescue WebPush::ExpiredSubscription
        Rails.logger.info "Removing expired WebPush subscription"
        subscription.destroy
      rescue WebPush::InvalidSubscription
        Rails.logger.info "Removing invalid WebPush subscription"
        subscription.destroy
    end
  end

  # You may override this method to validate options for the delivery method
  # Invalid options should raise a ValidationError
  #
  # def self.validate!(options)
  #   raise ValidationError, "required_option missing" unless options[:required_option]
  # end
end
