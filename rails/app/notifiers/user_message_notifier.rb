class UserMessageNotifier < Noticed::Event

  deliver_by :web_push, class: "DeliveryMethods::WebPush"
  deliver_by :email do |config|
      config.mailer = "GroupMailer"
      config.method = :receipt
      config.wait = 1.hours
  end

  # Add required params
  #
  #param :message

  # Define helper methods to make rendering easier.
  #
  # def message
  #   t(".message")
  # end
  #
  # def url
  #   post_path(params[:post])
  # end
end
