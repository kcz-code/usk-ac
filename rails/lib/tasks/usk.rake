namespace :usk do
  desc "Create admin"
  task :admin => ["db:setup", :environment] do
    User.new(name: "admin",
             email: "admin@usk-ac.de",
             user: true,
             admin: true,
             password: 'admin123',
             password_confirmation: 'admin123').save

  end

  desc "Crate test users"
  task :users => ["db:setup", :environment] do
    ['James Bond', 'Superman', 'Sherlock Holmes', 'Harry Potter'].each do |name|
      short = name.split(' ').first.downcase

      User.new(name: name,
      email: "#{short}@usk-ac.de",
      user: true,
      admin: false,
      password: "#{short}123",
      password_confirmation: "#{short}123").save
    end
  end

  desc "Create groups"
  task :groups => ["db:setup", :environment] do
    ['Sketchwalk', 'Treff', 'Sonstige'].each do |name|
      Group.new(name: name, description: "#{name} - Gruppe").save
    end
  end

  # desc "Create group mails"
  # task :mails => ["db:setup", :environment] do
  #   ['Mail Sketchwalk', 'Mail Treff', 'Mail Sonstige'].each do |name|
  #     GroupMail.new(subject: name, content: "Mail für #{name}", user_id: 1, mail_list_id: 1).save
  #   end
  # end

  desc "Create admin/users/mail_lists for tests"
  task :all do
    Rake::Task["usk:admin"].invoke
    Rake::Task["usk:users"].invoke
    Rake::Task["usk:groups"].invoke
#    Rake::Task["usk:mails"].invoke
  end

  task :daily_cleanup => [:environment] do
    # delete user -> user messages older then 7 days
    Message.where("created_at < ? and recipient_id is not NULL", 7.days.ago).destroy_all
    # delete user -> group messages older then 30 days
    Message.where("created_at < ? and group_id is not NULL", 30.days.ago).destroy_all
    # delete notifications older then 7 days
    Notification.where("created_at < ?", 7.days.ago).destroy_all
    # delete logs older then 3 months days
    Log.where("created_at < ?", 90.days.ago).destroy_all
  end

  # task :hourly_cleanup => [:environment] do
  #   # delete user -> user messages older then 7 days
  #   Message.where("created_at < ? and recipient_id is not NULL", 7.days.ago).destroy_all
  #   # delete user -> group messages older then 30 days
  #   Message.where("created_at < ? and group_id is not NULL", 30.days.ago).destroy_all
  # end

end
