class RenameMailListIdToGroupIdInMessages < ActiveRecord::Migration[7.0]
  def change
    rename_column :messages, :mail_list_id, :group_id
  end
end
