class AddLastSeenMessageIdToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :last_seen_message_id, :bigint, :default => 0
  end
end
