class RenameMailListsToGroups < ActiveRecord::Migration[7.0]
  def change
    drop_table :mail_list_users
    rename_table :mail_lists, :groups
  end
end
