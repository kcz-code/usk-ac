class AddSubjectToGroupMails < ActiveRecord::Migration[7.0]
  def change
    add_column :group_mails, :subject, :string
  end
end
