class AddConfirmAtToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :confirm_at, :datetime
  end
end
