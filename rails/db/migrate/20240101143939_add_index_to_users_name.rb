class AddIndexToUsersName < ActiveRecord::Migration[7.0]
  def change
    add_index :users, :name, unique: true
    remove_column :users, :short_name
  end
end
