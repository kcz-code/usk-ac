class DropGroupMailsCreateMessages < ActiveRecord::Migration[7.0]
  def change
    drop_table :group_mails

    create_table :messages do |t|
      t.string :content
      t.timestamps

      t.belongs_to :user
      t.belongs_to :mail_list, optional: true 
      t.belongs_to :recipient, class_name: 'User', optional: true 
    end
  end
end
