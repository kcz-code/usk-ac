class CreateMailListUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :mail_list_users do |t|
      t.belongs_to :user
      t.belongs_to :mail_list
      t.timestamps
    end
  end
end
