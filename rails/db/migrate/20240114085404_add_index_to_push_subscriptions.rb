class AddIndexToPushSubscriptions < ActiveRecord::Migration[7.0]
  def change
    add_index :push_subscriptions, :endpoint, unique: true
  end
end
