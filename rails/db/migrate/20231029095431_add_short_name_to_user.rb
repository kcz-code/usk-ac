class AddShortNameToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :short_name, :string
    User.find_each do |user|
      user.update_attribute(:short_name, user.name.downcase) 
    end
  end
end
