class RemoveLastSeenMessageIdFromUsers < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :last_seen_message_id
  end
end
