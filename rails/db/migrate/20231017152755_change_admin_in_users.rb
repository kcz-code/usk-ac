class ChangeAdminInUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :role, :string
    User.find_each do |user|
      user.update_attribute(:role, user.admin ? 'admin' : nil) 
    end
    remove_column :users, :admin
  end
end
