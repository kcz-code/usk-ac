class AddIndexToUsersShortName < ActiveRecord::Migration[7.0]
  def change
    add_index :users, :short_name, unique: true
  end
end
