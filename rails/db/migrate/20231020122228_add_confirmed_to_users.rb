class AddConfirmedToUsers < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :role, :string
    add_column :users, :confirmed, :boolean
    add_column :users, :user, :boolean, default: true
    add_column :users, :admin, :boolean, default: false
    add_column :users, :confirm_hash, :string
  end
end
