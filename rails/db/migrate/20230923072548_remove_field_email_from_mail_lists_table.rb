class RemoveFieldEmailFromMailListsTable < ActiveRecord::Migration[7.0]
  def change
    remove_column :mail_lists, :email
  end
end
