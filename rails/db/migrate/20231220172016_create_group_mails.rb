class CreateGroupMails < ActiveRecord::Migration[7.0]
  def change
    create_table :group_mails do |t|
      t.string :content
      t.timestamps

      t.belongs_to :user
      t.belongs_to :mail_list
    end
  end
end
