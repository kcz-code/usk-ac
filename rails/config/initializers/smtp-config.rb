Rails.application.configure do
  config.action_mailer.smtp_settings = {
    address:         ENV['SMTP_ADDRESS'],
    port:            ENV['SMTP_PORT'].to_i,
    domain:          ENV['SMTP_DOMAIN'],
    user_name:       ENV['SMTP_USER'],
    password:        ENV['SMTP_PASSWORD'],
    authentication:  'plain',
    enable_starttls: true,
    open_timeout:    5,
    read_timeout:    5,
    raise_delivery_errors: true
  }
end
