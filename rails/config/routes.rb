Rails.application.routes.draw do
  get    '/service_worker', to: 'service_worker#service_worker'
  resources :push_subscriptions, only: :create
  resources :groups
  resources :users
  resources :messages
  get    '/messages/group/:id/new', to: 'messages#group_new', as: :new_group_message
  get    '/names_search/:query', to: 'names_search#search'
  get    '/groups/:id/user/:user_id', to: 'groups#add_remove_user', as: :user_in_group
  get    '/users/:id/accept', to: 'users#accept', as: :accept_user
  get    '/register' => 'register#new'
  post   '/register' => 'register#create'
  get    '/confirm/:id'  => 'register#confirm'
  get    '/password/:id'  => 'password#edit'
  post   '/password'  => 'password#update'
  get    '/login'    => 'sessions#new'
  post   '/login'    => 'sessions#create'
  delete '/logout'   => 'sessions#destroy'
  get    '/administration' => 'sessions#administration'
  get    '/impressum' => 'impressum#index'
  get    '/datenschutz' => 'impressum#datenschutz'
  get    '/logs'  => 'logs#index'
  root   'home#index'
end
  