require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get login" do
    get login_url
    assert_response :success
  end

  test "should get signin" do
    get signin_url
    assert_response :success
  end

end
