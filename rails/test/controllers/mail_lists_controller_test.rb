require "test_helper"

class MailListsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get mail_lists_new_url
    assert_response :success
  end

  test "should get index" do
    get mail_lists_index_url
    assert_response :success
  end

  test "should get show" do
    get mail_lists_show_url
    assert_response :success
  end

  test "should get create" do
    get mail_lists_create_url
    assert_response :success
  end
end
