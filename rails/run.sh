#!/bin/bash

set -e

rails db:create
rails db:migrate
rails assets:precompile

printenv >> /etc/environment
service cron start

exec rails server -b 0.0.0.0
