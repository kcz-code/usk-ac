require 'uri'
require 'net/http'

STDOUT.sync = true

$ip_uri = URI('https://ipinfo.io/ip')
$old_ip = ''
$dyndns_str = ENV['DDNS_URI']
$delay = ENV['DDNS_INTERVAL'].to_i

def update(ip)
  if ip != $old_ip
    str = $dyndns_str.sub '[IP]', ip
    uri = URI(str)
    res = Net::HTTP.get_response(uri)
    if res.is_a?(Net::HTTPSuccess)
      $old_ip = ip
      puts res.body
      puts "DynDNS updated to #{ip}"
    end
  else
    puts 'IP not changed'
  end
end

while true
  res = Net::HTTP.get_response($ip_uri)
  update(res.body.strip) if res.is_a?(Net::HTTPSuccess)
  sleep($delay + rand(60))
end
